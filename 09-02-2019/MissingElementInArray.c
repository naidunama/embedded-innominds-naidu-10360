/*************************************************************************************************************************

  N.V.V.S NAIDU    10360      naidu.nama@gmail.com      cell:7729955884

PURPOSE:

	 This program is to implement  the missing element in the array upto n-1 range
**************************************************************************************************************************/

#include<stdio.h>

int main(){
	int size;
	printf("Enter the array size\n");
	scanf("%d",&size);
	//declaring the variables
	int arr[size-1],total=0,total1=0;
	//logic for adding the total number array elements
	for(int i=1;i<=size;i++){
		total1 = total1+i;
	}
	//logic for adding the array elements
	for(int i=0;i<size-1;i++){
		printf("Enter the data int array \n");
		scanf("%d",&arr[i]);
		total = total+arr[i];
	}
	printf("Missing eleemt in the array is  %d\n",(total1-total));
	return 0;
}

/****************************************OUTPUT FOR THE ABOVE PROGRAM*******************************************************
Enter the array size
7
Enter the data int array 
1
Enter the data int array 
2
Enter the data int array 
3
Enter the data int array 
5
Enter the data int array 
6
Enter the data int array 
7
Missing eleemt in the array is  4
****************************************************************************************************************************/
