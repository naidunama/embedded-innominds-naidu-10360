/*----------------------------------------------------------------------------------------
 
N.V.V.S NAIDU      10360      naidu.nama@gmail.com      cell:7729955884

PURPOSE:

	-this programs gives the output for useddefined string fuctions
	1.strcpy
	2.strcat
	3.strstr
	4.strtok
-----------------------------------------------------------------------------------------*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

//this function delimits the string whenever there is a " " in the given string
void myToken(char *str1,char *str2) {

	int i=0,j=0;
	char * token=(char *)malloc(strlen(str1)*sizeof(char));
	for(i=0;i<strlen(str1);i=j+1) {
		if(token!=NULL)
			j=i;
		while(str1[j]!=str2[0])  {
			if(str1[j]=='\0') break;
			j++;
		}
		strncpy(token,str1+i,j-i);
		token[j-i]='\0';
		printf("%s",token);
		printf("\n");
	}





}
//this function attaches the conent of string2 at the end of string 1
char* myStrcat(char *str1,char *str2){
	int i=0,j=0;
	printf("**********Before concatination**********\n");
	printf("1st string is  = %s\n",str1);
	printf("2nd string is  = %s\n",str2);
	while(str1[i]!='\0'){
		i++;
	}
	while(str1[i-1]=str2[j]){
		i++;
		j++;
	}

	return str1;
}

//this function copies the string2 content to string1
char* myStrcpy(char *str1,char *str2){
	int i=0;
	printf("**********Before copy**********\n");
	printf("1st string is  = %s\n",str1);
	printf("2nd string is  = %s\n",str2);
	while(str2[i]!='\0'){
		str1[i]=str2[i];
		i++;
	}
	str1[i]='\0';
	return str1;
} 
//this function displays the operations that a user can perform
void menu() {
	printf("------------USERDEFINDED STRING MENU--------------\n1.stringconcatination\n2.stringcopy\n3.string serach in another string\n4.string token\n5.exit\n------------------------------------\n");
}
//this function finds if string2 present in string1 or not
int mystrstr(char *str1,char*str2) {

	char i,j=0,k;
	int flag=0,len,count,len1;
	len=strlen(str2)-1;
	len1=strlen(str1);
	for(i=0;str1[i];i++) {
		k=i;
		j=0;
		count=0;
		while( str1[i]==str2[j]) {
			count++;
			j++;
			i++;
			if(count==len) {
				printf("string 2 found in string 1 at index: \n%d\n",i-len);
				printf("\n");
				return 1;	
			}
		}
	}
	return 0;


}

int main(){
	char s1[50],s2[50],*s3;
	int choice;
	while(1) {
		menu();
		printf("enter your choice\n");
		scanf("%d",&choice);
		getchar();

		switch(choice) {

			case 1 :
				printf("enter the string 1\n");
				fgets(s1,50,stdin);
				printf("enter the string 2\n");
				fgets(s2,50,stdin);
				s3=myStrcat(s1,s2);
				printf("***********After Cancatination**********\n");
				printf("new string is :\n%s\n",s3);
				break;

			case 2:
				printf("enter the string 1\n");
				fgets(s1,50,stdin);
				printf("enter the string 2\n");
				fgets(s2,50,stdin);
				s3=myStrcpy(s1,s2);
				printf("***********After Copy**********\n");
				printf("string 1 after copy from string2 to string 1 is :\n%s\n",s3);
				break;

			case 3:
				printf("enter the string 1\n");
				fgets(s1,50,stdin);
				printf("enter the string 2\n");
				fgets(s2,50,stdin);
				if(mystrstr(s1,s2))
					printf("Second string found in 1st string  \n");
				else
					printf("second string not found in first string\n");
				break;

			case 4:
				printf("enter the string 1\n");
				fgets(s1,50,stdin);
				char s4[2]=" ";
				printf("---------------------------------\n");
				myToken(s1,s4);
				break;


			case 5:exit(0);
		}
	}
	return 0;
}
/********************************OUTPUT************************************


------------USERDEFINDED STRING MENU--------------
1.stringconcatination
2.stringcopy
3.string serach in another string
4.string token
5.exit
------------------------------------
enter your choice
1
enter the string 1
naidu
enter the string 2
nama
**********Before concatination**********
1st string is  = naidu

2nd string is  = nama

***********After Cancatination**********
new string is :
naidunama

------------USERDEFINDED STRING MENU--------------
1.stringconcatination
2.stringcopy
3.string serach in another string
4.string token
5.exit
------------------------------------
enter your choice
2
enter the string 1
naidu
enter the string 2
nadunama 
**********Before copy**********
1st string is  = naidu

2nd string is  = nadunama

***********After Copy**********
string 1 after copy from string2 to string 1 is :
nadunama

------------USERDEFINDED STRING MENU--------------
1.stringconcatination
2.stringcopy
3.string serach in another string
4.string token
5.exit
------------------------------------
enter your choice
3
enter the string 1
this is innominds
enter the string 2
innominds
string 2 found in string 1 at index: 
8

Second string found in 1st string  

------------USERDEFINDED STRING MENU--------------
1.stringconcatination
2.stringcopy
3.string serach in another string
4.string token
5.exit
------------------------------------
enter your choice
4
enter the string 1
this is innominds
---------------------------------\n
this
is
innominds

------------USERDEFINDED STRING MENU--------------
1.stringconcatination
2.stringcopy
3.string serach in another string
4.string token
5.exit
------------------------------------
enter your choice
5

**********************************************************************/
