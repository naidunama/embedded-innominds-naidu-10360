/*----------------------------------------------------------------------------------------------------------------------------------
      
	N.V.V.S NAIDU	10360	naidu.nama@gmail.com		Cell : 7729955884


       PURPOSE
       ---------
     	-The program mainly deals with stack operations which works on principle LIFO where adding at first  position will be deleted last  and displaying the conent in the stack

---------------------------------------------------------------------------------------------------------------------------------
*/
#include<iostream>
#include<stdlib.h>
using namespace std;
struct stack {
	int data;
	stack *next;
};

class StackOperations {

	stack *head;
	public:
//default constructor
	StackOperations() {

		head=NULL;
	}
//this methos adds the elements into stack
	void push(int data)
	{
		stack *newnode=new stack;
		newnode->data=data;
		newnode->next=NULL;
		if(head==NULL)
		{
			head=newnode;
			return;
		}
		stack *temp;
		for(temp=head;temp->next!=NULL;temp=temp->next);
		temp->next=newnode;
		return;
	}
//this method displays the contents in the stack
	void displayStackItems() 
	{
		if(head==NULL)
			cout<<"stack underflow"<<endl;
		else
		{
			stack *temp;
			for(temp=head;temp!=NULL;temp=temp->next)
				cout<<temp->data<<" ";
		}
		cout<<endl;
	}
//this method deletes the last element from the stack and returns the deleted data
	int pop()
	{
		stack *temp,*prev;
		int data;
		if(head==NULL) 
		{
			cout<<"stack is empty"<<endl;
			return 0;
		}
		if(head->next==NULL) {
			data=head->data;
			head=NULL;
			return data;
		}
		for(temp=head;temp->next!=NULL;prev=temp,temp=temp->next);
		prev->next=NULL;
		data=temp->data;
		delete temp;
		return data;
	}

//destructor
	~StackOperations() {
		 
		delete head;
	}
//this method displays the operations to be performed on the stack
	void menu()
	{
		int choice;
		int data;
		while(1) {
			cout<<"*********MENU********"<<endl;
			cout<<"1.push into stack"<<endl<<"2.pop from the stack"<<endl<<"3.display"<<endl<<"4.exit"<<endl;
			cout<<"*********************"<<endl;
			cout<<"enter ur choice:"<<endl;
			cin>>choice;
			switch(choice) 
			{
				case 1:
					cout<<"enter the data to be push"<<endl;
					cin>>data;
					push(data);
					break;
				case 2:
					data=pop();
				
					cout<<"poped value from the stack is:"<<data<<endl;
					cout<<"****************************"<<endl;
					break;
				case 3:
					cout<<"****************************"<<endl;
					cout<<"displaying the stack elements "<<endl;
					displayStackItems();
					cout<<"****************************"<<endl;
					break;
				case 4:
					exit(0);

				default:
					cout<<"invalid input"<<endl;
			}
		}
	}
};
int main() {

//creating an object for the stackoperations
	StackOperations s;
//calling menu method
	s.menu();
	return 0;
}

/*================================================================================

OUTPUT:
*********MENU********
1.push into stack
2.pop from the stack
3.display
4.exit
*********************
enter ur choice:
1
enter the data to be push
9
*********MENU********
1.push into stack
2.pop from the stack
3.display
4.exit
*********************
enter ur choice:
1
enter the data to be push
8
*********MENU********
1.push into stack
2.pop from the stack
3.display
4.exit
*********************
enter ur choice:
1
enter the data to be push
7
*********MENU********
1.push into stack
2.pop from the stack
3.display
4.exit
*********************
enter ur choice:
3
****************************
displaying the stack elements 
9 8 7 
****************************
*********MENU********
1.push into stack
2.pop from the stack
3.display
4.exit
*********************
enter ur choice:
2
poped value from the stack is:7
****************************
*********MENU********
1.push into stack
2.pop from the stack
3.display
4.exit
*********************
enter ur choice:
3
****************************
displaying the stack elements 
9 8 
****************************
*********MENU********
1.push into stack
2.pop from the stack
3.display
4.exit
*********************
enter ur choice:
4

*/

