
/****************************************************************************************************** 
  	
	
  	
	N.V.V.S NAIDU	10360	naidu.nama@gmail.com		Cell : 7729955884

        Purpose:
        --------

        --this programe deals with sorting of an array using bubblesort alogrithm

******************************************************************************************************/
#include<iostream>
#include<stdlib.h>
using namespace std;

class BubbleSort {

	public :

		void inputArray(int *array,int size) {
			cout<<"enter the elements into array"<<endl;
			for(int i=0;i<size;i++) {
				cin>>array[i];
			}

		}




		void bubbleSort(int *array,int size) {

			int temp;
			for(int i=0;i<size;++i)
			{
				for(int j=0;j<size-1-i;++j)
					if(array[j]>array[j+1])
					{
						temp=array[j];
						array[j]=array[j+1];
						array[j+1]=temp;
					}
			}

		}
		void displayArrayElements(int *array,int size) {
			for(int i=0;i<size;i++) {
				cout<<array[i]<<" ";
			}
			cout<<endl;
		}

};

int main() {

	int *array=NULL;

	int size;
	cout<<"enter the size of array "<<endl;
	cin>>size;
	while(size<=0) {
	cout<<"array size cannot be negative and 0......please renter "<<endl;
	cin>>size;
	}
	array=new int[size];
	BubbleSort b;
	b.inputArray(array,size);
	cout<<"array elements before sorting "<<endl;
	b.displayArrayElements(array,size);
	b.bubbleSort(array,size);
	cout<<"array elements after Bubble sort "<<endl;
	b.displayArrayElements(array,size);
}
/*********************************************************************************************************
OUTPUT:
-------

enter the size of array 
5
enter the elements into array
9
1
4
2
8
array elements before sorting 
9 1 4 2 8 
array elements after Bubble sort 
1 2 4 8 9 
*/


