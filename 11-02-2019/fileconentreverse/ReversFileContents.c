/*****************************************************************************************
 
N.V.V.S NAIDU    10360     naidu.nama@gmail.com      cell:7729955884

PURPOSE:

	this programe deals with files,where the content of input file should be reversed and placed in output file 

******************************************************************************************/
#include <stdio.h>
#include <stdlib.h>

int main(void) {

	FILE *fp1=NULL,*fp2=NULL;
	int ch,len;
	//opening input file to read
	if((fp1 = fopen("input.txt","r")) == NULL) {
		printf("failed to open the input file........\n");
		return -1;
	}
	else
		printf("INPUT FILE HAS  OPENED SUCCESSFULLY\n");
	//opening outputfile to write
	if((fp2 = fopen("Output.txt","w")) == NULL) {
		printf("failed to open the output file........\n");
		return -1;
	}
	else
		printf("OUTPUT FILE HAS OPENED SUCCESSFULLY\n");
	printf("------------------------------------\n");
	printf("printing the contents of input file \n");
	while((ch = fgetc(fp1)) != EOF){
		printf("%c",ch);
	}
	printf("------------------------------------\n");
	fseek(fp1,-1,SEEK_END);
	len = ftell(fp1);
	printf("the size of input file is  %d\n",len);
	while(len >= 0){
		ch = fgetc(fp1);
		fputc(ch,fp2);
		fseek(fp1,-2,SEEK_CUR);
		len--;
	}
	fclose(fp2);
	fp2 = fopen("Output.txt","r");
	printf("------------------------------------\n");
	printf("printing the contents of output file \n");
	while((ch = fgetc(fp2)) != EOF){
		printf("%c",ch);
	}
	printf("\n");
	printf("------------------------------------\n");
	//closing the files 
	fclose(fp1);
	fclose(fp2);
	return 0;
}

/***********************OUTPUT******************************

INPUT FILE HAS  OPENED SUCCESSFULLY
OUTPUT FILE HAS OPENED SUCCESSFULLY
------------------------------------
printing the contents of input file 
Hello Veera Venkata Naidu Nama
Please see your Innominds Email and Myspace Portal login details
thank you
------------------------------------
the size of input file is  105
------------------------------------
printing the contents of output file 

uoy knaht
sliated nigol latroP ecapsyM dna liamE sdnimonnI ruoy ees esaelP
amaN udiaN atakneV areeV olleH
------------------------------------
*****************************************************************/

