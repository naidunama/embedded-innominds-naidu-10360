/********************************************************************************************
 N.V.V.S NAIDU     10360    naidu.nama@gmail.com         cell:7729955884

PURPOSE:

	-this programe deals with to copy the conents of two files into the third file

*****************************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main(void) {

	FILE *fp1=NULL,*fp2=NULL,*fp3=NULL;
	int ch;
	if((fp1=fopen("file1.txt","rw")) == NULL) {
		printf("Failed to open the file.........\n");
		return -1;
	}
	else
		printf("File1 has  Opened succesfully\n");

	if((fp2=fopen("file2.txt","rw")) == NULL) {
		printf("Failed to open the  file.........\n");
		return -1;
	}
	else
		printf("File2 has  Opened succesfully\n");

	if((fp3=fopen("output.txt","a+")) == NULL) {
		printf("Failed to open the file..........\n");
		return -1;
	}
	else
		printf("output file has Opened succesfully\n");

	while((ch = fgetc(fp1)) != -1) {
		fputc(ch,fp3);
	}
	while((ch = fgetc(fp2)) != -1) {
		fputc(ch,fp3);
	}
	printf("data written successsfully from file 1 and file 2 to output file \n");
	//closing all the files whic are opened 

	fclose(fp1);
	fclose(fp2);
	fclose(fp3);

	return 0;

}

/*****************OUTPUT************************************

File1 has  Opened succesfully
File2 has  Opened succesfully
output file has Opened succesfully
data written successsfully from file 1 and file 2 to output file 
****************************************************************/
