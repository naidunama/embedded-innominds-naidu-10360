/****************************************************************************************************** 
  	
	
  	
	N.V.V.S NAIDU	10360	naidu.nama@gmail.com		Cell : 7729955884

        Purpose:
        --------

        --To Communicate between server and client using sharedmemory and threads
        --client uses pthreads and mutex concept

******************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <iostream> 
#include <sys/ipc.h> 
#include <sys/shm.h> 
#include <unistd.h>
using namespace std;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
static int shmid;
static char *name;
//creating client class
class Client {
	pthread_t thread1, thread2,thread3;
	

	public :
	// creating the Default constructor for client
	Client() {

		
		key_t key = ftok("myfile1",85);

		shmid = shmget(key,1024,0666|IPC_CREAT); 


		char  *str = (char*) shmat(shmid,(void*)0,0); 

		cout<<str<<endl;
		shmdt(str);
	}
	//method for entering the clientsname
	static void *clientName(void *ptr) {
		pthread_mutex_lock( &mutex );
		cout<<"enter client name :"<<endl;
		name=(char*) shmat(shmid,(void*)0,0); 
		cin>>name;
		sleep(10);
		pthread_mutex_unlock( &mutex );
		

	}
	//metod to create three client threads
	void create_clientThread() {
		pthread_create( &thread1, NULL,clientName,NULL);
		pthread_create( &thread2, NULL,clientName,NULL);
		pthread_create( &thread3, NULL,clientName, NULL);

        }
	//method for joining the three threads
	void joinThreads() {
		pthread_join( thread1, NULL);
    		pthread_join( thread2, NULL);
		pthread_join( thread3, NULL);

	}	


};

int main()
{
	//creating the instance of client class
    	Client c;
	//calling the clientthread method
	c.create_clientThread();
	sleep(10);
	//calling the joiningclientthread methid
	c.joinThreads();
	return 0; 

}
/*================================================================================
// OutPut
//---------------------------

hey whats ur name
enter client name :
naidu
enter client name :
rahul
enter client name :
raju
*/

