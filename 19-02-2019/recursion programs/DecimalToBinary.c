/***************************************************************************************

N.V.V.S NAIDU     10360       naidu.nama@gmail.com    cell:7729955884 

PURPOSE:
	~ this program is to convert decimal number to equivalent binary number  using recurssion
*****************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
//it converts given decimal number in  binary number and returns the value  
int decimalToBinary(int num) {
	if(num==0) 
		return 0;
	else 
		return (num%2+10 * (decimalToBinary(num/2)));

}

int main() {
	int num;
	printf("enter the number\n");
	scanf("%d",&num);
	int a=decimalToBinary(num);
	printf("the binary  number is %d  for the given decimal number  %d\n",a,num);
	return 0;
}
/************************OUTPUT****************************************
 enter the number
32
the binary  number is 100000  for the given decimal number  32
************************************************************************/
