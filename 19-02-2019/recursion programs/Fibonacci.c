/***************************************************************************************

N.V.V.S NAIDU     10360       naidu.nama@gmail.com    cell:7729955884 

PURPOSE:
	~ this program is to find fibonacci series upto given number using recurssion
*****************************************************************************************/
#include<stdio.h> 
//this function finds the fibonacci series for the given number
int Fibo(int n)
{
	if ( n == 0 )
		return 0;
	else if ( n == 1 )
		return 1;
	else
		return ( Fibo(n-1) + Fibo(n-2) );
} 

int main () 
{ 

	int num,i=0;
	printf("enter the number to find the fibonacci series :\n");
	scanf("%d",&num);
	while(num<0) {
		printf("fibonacci number  cannot be negative......re enter \n");
		scanf("%d",&num);
	}

	printf("Fibonacci series for the given number is \n");

	for (int j = 1 ; j <= num ; j++ )
	{
		printf("%d ", Fibo(i));
		i++; 
	}
	printf("\n");

	return 0;
}

/*********************************OUTPUT*************************************

enter the number to find the fibonacci series :
10
Fibonacci series for the given number is 
0 1 1 2 3 5 8 13 21 34 
*******************************************************************************/
