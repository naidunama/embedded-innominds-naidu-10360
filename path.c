/*----------------------------------------------------------------------------------------------------------------------------------

  N.V.V.S NAIDU	10360	naidu.nama@gmail.com		Cell : 7729955884


  PURPOSE
  ---------
  This program is typically to find the no of possible ways to reach the last element from the first element in forward direction

  ----------------------------------------------------------------------------------------------------------------------------------*/


#include<stdio.h>
int main() {

	int row;
	int column;
	int i,j;
	printf("enter how many rows in a matrix \n");
	scanf("%d",&row);
	while(row<=0) {
		printf("in a matrix rows cannot be negative or 0,please re enter \n");
		scanf("%d",&row);
	}
	printf("enter how many columns in a matrix \n");
	scanf("%d",&column);
	while(column<=0) {
		printf("in a matrix column cannot be negative or 0,please re enter \n");
		scanf("%d",&column);
	}
	int a[row][column];
	for(i=0,j=0;j<column;j++)
	{
		a[i][j]=1;
	}
	for(j=0,i=0;i<row;i++)
	{
		a[i][j]=1;
	}
	for(i=0;i<row-1;i++)
	{
		for(j=1;j<column;j++)
		{
			a[i+1][j]=a[i+1][j-1]+a[i][j];
		}


	}
	printf("the possible forward direction in a given %d rows and %d columns is %d \n",row,column,a[row-1][column-1]);
	return 0;
}
/********************output**********************
 
enter how many rows in a matrix
4
enter how many columns in a matrix
7
the possible forward direction in a given 4 rows and 7 columns matrix is 84

*/

