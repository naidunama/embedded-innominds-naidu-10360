/******************************************************************************************

  N.V.V.S NAIDU     10360    naidu.nama@gmail.com     cell:7729955884   

PURPOSE :

-this programe removes the extra occurance alphabets in the given string

 *********************************************************************************************/
#include<stdio.h>
#include<string.h>
//to remove the extra occurences in a string
void repeated(char *p)
{
	int i,j,cnt=0;
	char temp;
	for(i=0;p[i];i++)
	{
		temp=p[i];
		for(j=i+1;p[j];j++) {

			if(p[j]==temp) {
				memmove(p+j,p+j+1,strlen(p+j+1)+1);
			}
		}
	}

	printf("the new string without repeated is\n%s \n",p);
}

void main()
{
	char str1[30];
	printf("enter the input for string1 \n");
	gets(str1);
	repeated(str1);
}
/********************OUTPUT*************************

  enter the input for string1
  this is innominds
  the new string without repeated is
  this nomd
 ****************************************************/
