/*---------------------------------------------------------------------------------------------------

N.V.V.S NAIDU       10360       naidu.nama@gmail.com         cell:779955884

PURPOSE:

	-the programe deals with the finding of roots for a given 2 degreee qudratic coefficients 
------------------------------------------------------------------------------------------------------*/

#include<stdio.h>
#include<math.h>
int main() {
	int a,b,c;
	float root1,root2;
	printf("enter the coefficients of qudratic equation \n");
	scanf("%d %d %d",&a,&b,&c);
	while(a==0) {
		printf("highest coefficient cannot be zero....please renter \n");
		scanf("%d",&a);
	}
	float r=(b*b)-4*a*c;
	if(r<0) {
		r=-r;
		root1=(float)-b/(2*a);
		printf("%f\n",root1);
		root2=sqrt(r)/(2*a);
		printf("the 1st complex  root of equation is %f+i%f\n",root1,root2);
		printf("the 2nd complex root of equation is %f-i%f\n",root1,root2);
	} else {


		root1=(-b+sqrt(r))/(2*a);
		root2=(-b-sqrt(r))/(2*a);
		printf("the 1st root of equation is %f\n",root1);
		printf("the 2nd root of equation is %f\n",root2);
	}
	return 0;
}

/*********************OUTPUT*************************************8
 
enter the coefficients of qudratic equation 
6  
9
2
the 1st root of equation is -0.271286
the 2nd root of equation is -1.228714

$ ./a.out

enter the coefficients of qudratic equation 
1 
0
8
the 1st complex  root of equation is 0.000000+i2.828427
the 2nd complex root of equation is 0.000000-i2.828427

$ ./a.out
enter the coefficients of qudratic equation 
0
2
4
highest coefficient cannot be zero....please renter 
-4
the 1st root of equation is -0.780776
the 2nd root of equation is 1.280776
**********************************************************************/

