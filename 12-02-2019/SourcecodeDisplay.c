/**************************************************************************************************
 
N.V.V.S NAIDU     10360       naidu.nama@gmail.com        cell:7729955884

PURPOSE:
	-ths programe gives the output along with content present in source code 
***********************************************************************************************/
#include <stdio.h>
#include <stdlib.h>

int main(void) {

	FILE *fp = NULL;
	int ch;
	fp = fopen(__FILE__,"r");	
	//This give the file name which is we were given.
	//printf("%s\n",__FILE__);
	if(fp == NULL) {
		perror("fp");
		return -1;
	}
	printf("FILE OPENED SUCCESSFULLY\n");
	printf("my name is naidu \n");
	printf("------------------------------------------------\n");
	while((ch=fgetc(fp)) != -1) {
		printf("%c",ch);
	}
	fclose(fp);
	return 0;
}
/***************************OUTPUT******************************
 
FILE OPENED SUCCESSFULLY
my name is naidu
------------------------------------------------
#include <stdio.h>
#include <stdlib.h>

int main(void) {

	FILE *fp = NULL;
	int ch;
	fp = fopen(__FILE__,"r");	
	//This give the file name which is we were given.
	//printf("%s\n",__FILE__);
	if(fp == NULL) {
		perror("fp");
		return -1;
	}
	printf("FILE OPENED SUCCESSFULLY\n");
	printf("my name is naidu \n");
	printf("------------------------------------------------\n");
	while((ch=fgetc(fp)) != -1) {
		printf("%c",ch);
	}
	fclose(fp);
	return 0;
}
****************************************************************/
