/****************************************************************************************************** 



  N.V.V.S NAIDU	10360	naidu.nama@gmail.com		Cell : 7729955884

Purpose:
--------

--Enter n number of student details and display all student details

 ******************************************************************************************************/
#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <string>
#include <bits/stdc++.h>
using namespace std;


class Student  {


	int student_id;
	string student_name;
	int student_contact;
	string student_branch;
	public:	
	Student() {

		cout<<"enter student id :"<<endl;
		cin>>student_id;
		cout<<"enter student name :"<<endl;
		cin>>student_name;
		cout<<"enter student contact :"<<endl;
		cin>>student_contact;
		cout<<"enter student branch(EEE/ECE/CSE/MECH/CIVIL/IT) :"<<endl;
		cin>>student_branch;

	}
	string getBranch() {
		return student_branch;
	}		
	void displayStudentDetails() {
		cout<<"\t\tstudent_id:"<<student_id<<endl;
		cout<<"\t\tstudent name:"<<student_name<<endl;
		cout<<"\t\tstudent contact:"<<student_contact<<endl;
		cout<<"\t\tstudent branch:"<<student_branch<<endl;

	}
};
//creating university class 
class University {

	//declaring the data members
	string university_name;
	vector<Student> s;
	public:


	//default constructor for University class
	University() {

		cout<<"enter student university(geetham/andhra/jntuk):"<<endl;
		cin>>university_name;	
	}
	void 	addStudent(Student &stu) {
		s.push_back(stu);
	}
	//getters for university name and branch
	string getUniversityName()
	{ 
		return university_name;
	}


	//displaying students information
	void displayUniversityDetails() {

		cout<<"\t\tuniversity name:"<<university_name<<endl;
		for(size_t i=0; i <s.size(); ++i) {
			s[i].displayStudentDetails();
			cout<<"\t\t-------------------------\n"<<endl;
		}


	}




};

//method to search by university-name/branch


//main function 
int main() {

	//declaring the local variables
	int choice;
	string name;

	vector<University> uni;



	while(1) {
		//displaying the menu page
		cout<<"1.add student\n2.display all students\n3.quit"<<endl;

		//reading the user's choice
		cout<<"enter your choice:"<<endl;
		cin>>choice;

		//navigating according to user's choice		
		switch(choice) {


			case 1:
				{
					University u;
					Student stu;
					u.addStudent(stu);
					uni.push_back(u);

				}
				break;
			case 2:
				for ( auto it : uni) { 

					cout<<"\t\t-------------------------\n"<<endl;
					it.displayUniversityDetails();



				}

				break;

			case 3:
				exit(0);
		}



	}    
	return 0;
}
/*================================================================================
// OutPut
//---------------------------

1.add student
2.display all students
3.quit
enter your choice:
1
enter student university(geetham/andhra/jntuk):
geetham
enter student id :
1
enter student name :
naidu
enter student contact :
77299
enter student branch(EEE/ECE/CSE/MECH/CIVIL/IT) :
EEE
1.add student
2.display all students
3.quit
enter your choice:
1
enter student university(geetham/andhra/jntuk):
andhra
enter student id :
2
enter student name :
ravi 
enter student contact :
99999
enter student branch(EEE/ECE/CSE/MECH/CIVIL/IT) :
CSE
1.add student
2.display all students
3.quit
enter your choice:
2
-------------------------

university name:geetham
student_id:1
student name:naidu
student contact:77299
student branch:EEE
-------------------------

-------------------------

university name:andhra
student_id:2
student name:ravi
student contact:99999
student branch:CSE
-------------------------

1.add student
2.display all students
3.quit
enter your choice:
3


 */





