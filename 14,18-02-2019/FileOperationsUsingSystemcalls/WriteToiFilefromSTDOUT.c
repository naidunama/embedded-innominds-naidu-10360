/***************************************************************************************

N.V.V.S NAIDU     10360       naidu.nama@gmail.com    cell:7729955884 

PURPOSE:
	~ this program is to copy the conents entered on STDOUT to file
*****************************************************************************************/
#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>

int main() {

	int fp1,size;
	char buff[100];
	fp1=open("output1.txt",O_WRONLY | O_CREAT);
	if (fp1 == -1)

		printf("Error in opening the file  \n"); 
	else 
			printf("input file opened in read mode\n");
	printf("write the conent that should be written into file\n");
	printf("..............press (CNTRL+c) to exit the loop .......\n");
	while(1) {
	size=read(0,buff,sizeof(buff));
	//writing from input file to output file
	write(fp1,buff,size);
	}
	printf("writing to output file using system call successfull\n");
	close(fp1);
	return 0;

}

/********************8OUTPUT****************************************
input file opened in read mode
write the conent that should be written into file
..............press (CNTRL+c) to exit the loop .......
my name is naidu nama
how are you 
im fine
thank you
bye
^C
~/ctraining/programs$ chmod 660 output1.txt 
~/ctraining/programs$ cat output1.txt 
my name is naidu nama
how are you 
im fine
thank you
bye

**********************************************************************/

