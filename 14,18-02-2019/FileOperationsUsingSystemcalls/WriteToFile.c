/***************************************************************************************

N.V.V.S NAIDU     10360       naidu.nama@gmail.com    cell:7729955884 

PURPOSE:
	~ this program is to copy the content of file1 to file2 using system programming
*****************************************************************************************/
#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>

int main() {

	int fp1,fp2;
	int size=100;
	fp1=open("input.txt",O_RDONLY);
	fp2=open("output.txt",O_WRONLY | O_CREAT);
	if (fp1 == -1)

		printf("Error in opening the file  \n"); 
	else 
			printf("input file opened in read mode\n");
	if(fp2 == -1) 
		printf("Error in creating  the file  \n");
	else 
		printf("output file opened to write the conents of input file \n");
	//writing from input file to output file
	while(size) {
	char buff[size];
	size=read(fp1,buff,size);
	write(fp2,buff,size);
	}
	printf("writing to output file using system call successfull\n");
	close(fp1);
	close(fp2);
	return 0;
}

/****************************OUTPUT******************************

input file opened in read mode
output file opened to write the conents of input file 
writing to output file using system call successfull

******************************************************************/




