#include"Client.h"

void error(char *msg){
	perror(msg);
	exit(1);
}

int main(int argc, char **argv){
	int sockfd,size,portNum;
	char *buff;
	struct hostent *server;
	struct sockaddr_in serv_addr;
	FILE *fp;
	if(argc<3){
		fprintf(stderr,"usage %s hostname port\n",argv[0]);
		exit(0);
	}
	portNum = atoi(argv[2]);
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0)error("Error openeing socket");
	server = gethostbyname(argv[1]);
	if(server == NULL){
		fprintf(stderr,"error,no such host\n");
		exit(0);
	}
	memset((char*) &serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	strncpy((char *)server->h_addr,(char *)&serv_addr.sin_addr.s_addr,server->h_length);
	serv_addr.sin_port = htons(portNum);
	if(connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0)
		error("ERROR IN CONNECTING");


	write(sockfd,argv[3],strlen(argv[3]));
	sleep(2);
	read(sockfd,&size,sizeof(int));
	buff=(char*)malloc((size)*sizeof(char));
	read(sockfd,buff,size);
	fp=fopen(argv[4],"w");
	fwrite(buff,size,1,fp);
	fclose(fp);
	close(sockfd);
	return 0;	
}
