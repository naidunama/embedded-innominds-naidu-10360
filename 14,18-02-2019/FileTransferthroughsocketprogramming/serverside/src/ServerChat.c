#include"Server.h"

void error(char *msg){
	perror(msg);
	exit(1);
}

int main(int argc, char **argv) {
	int sockfd,newSockfd,portNum,size;
	char buf[1024];
	char *buff;
	FILE *fp;
	int flag=-1;
	struct sockaddr_in serv_addr, cli_addr;
	socklen_t cliLen;
	if(argc<2){
		fprintf(stderr,"ERROR, no port available\n");
		exit(1);
	}
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0)error("Error openeing socket");
	memset((char*) &serv_addr,0,sizeof(serv_addr));
	portNum = atoi(argv[1]);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portNum);
	if(bind(sockfd,(struct sockaddr*) &serv_addr, sizeof(serv_addr))<0)
		error("ERROR ON BINDING");
	listen(sockfd,5);

	cliLen = sizeof(cli_addr);
	newSockfd = accept(sockfd,(struct sockaddr *) &cli_addr,&cliLen);
	if(newSockfd<0)
		error("ERROR IN ACCEPTING");

	read(newSockfd,buf,1024);
	fp=fopen(buf,"r");
	if(fp<0) {
		printf("can't open file:%s\n",buf);
		write(newSockfd,&flag,sizeof(int));
		exit(1);
	}
	fseek(fp,0,2);
	size=ftell(fp);
	rewind(fp);
	write(newSockfd,&size,sizeof(int));
	buff=(char*)malloc((size)*sizeof(char));
	fread(buff,size,1,fp);
	write(newSockfd,buff,strlen(buff));
	fclose(fp);
	printf("file transfer success \n");

	close(newSockfd);
	close(sockfd);
	return 0;
}
