//all the headers are included
#include"server.h"

//it serach the file in the given directory if found returns 1 else 0
int fileSearch(char *buff) {


	printf("displaying the files on server side\n");
	struct dirent *dir;
	DIR *d;
	d=opendir(".");
	while((dir=readdir(d))!=NULL) {
		printf("%s\n",dir->d_name);
		if(!(strcmp(dir->d_name,buff))) {
			return 1;
		}
	}
	return 0;
}





//programe starts here and it takes 2 arguments
int main(int argc,char **argv) {

	int sockfd,newSockfd,n;
	char buf[1024];
	char *buff="no such files";
	struct sockaddr_in serv_addr,cli_addr;
	socklen_t cliLen;
	if(argc!=2) {
		printf("enter the arguments in command lind correctly \n");
	}
	sockfd=socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0) {
		printf("error in creating the socket\n");
		exit(0);
	}
	memset((char *)&serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_family=AF_INET;
	serv_addr.sin_addr.s_addr=INADDR_ANY;
	serv_addr.sin_port=htons(atoi(argv[1]));
//it binds the connection
	if(bind(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0) {
		printf("error in binding \n");
		exit(0);
	}
//it listens to no of clients
	listen(sockfd,2);
	cliLen=sizeof(cli_addr);
//connection will hold here until newsockfod is greater than 0
	newSockfd=accept(sockfd,(struct sockaddr *)&cli_addr,&cliLen);
	if(newSockfd<0) {
		printf("error in establishing the connection\n");
		exit(0);
	}
	n=read(newSockfd,buf,sizeof(buf));
	if(n<0) {
		printf("there is reading pronblm on client side\n");
	}
	printf("%s\n",buf);
	int index=fileSearch(buf);
	if(index==0) {
		write(newSockfd,buff,strlen(buff));
	} else {
		buff="send file latter time is up";
		write(newSockfd,buff,strlen(buff));
	}
		

	

//closing sockets
//
	close(newSockfd);
	close(sockfd);
	return 0;
}













