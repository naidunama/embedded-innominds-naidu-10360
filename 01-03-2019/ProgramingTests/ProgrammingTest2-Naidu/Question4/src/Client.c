//all the headers are included
#include"client.h"

//the progarme starts with main it takes 3 arguments
int main(int argc,char **argv) {

	struct sockaddr_in serv_addr;
	int sockfd,n;
	struct hostent *server;
	char buf[1024];

	if(argc!=3) {
		printf("enter the inputs correctly in command line \n");
	}
//creates the socket
	sockfd=socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0) {
		printf("error in creating the socket\n");
		exit(0);
	}
//it assignes ip adress
	server=gethostbyname(argv[1]);
	memset((char *)&serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_family=AF_INET;
	strncpy(( char *)server->h_addr,(char *)&serv_addr.sin_addr.s_addr,server->h_length);
	serv_addr.sin_port=htons(atoi(argv[2]));
//it establish the connection between client and server
	if(connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0) {

		printf("error in establishing the connection\n");
		exit(0);
	}
	printf("enter the file name\n");
	memset(buf,0,sizeof(buf));
	n=read(0,buf,1024);
	if(n<0) {
		printf("there is a reading problem from client side \n");
	}
	n=write(sockfd,buf,sizeof(buf));
	if(n<0) {
		printf("there is a writing problem from client side \n");
	}
	read(sockfd,buf,sizeof(buf));
	printf("%s\n",buf);

	close(sockfd);
	return 0;
}


