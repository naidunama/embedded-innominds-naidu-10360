#include<stdio.h>
//this function search the element in the array if found it returns position else -1
int binarySearch(int array[],int low,int high,int data) {


	if(low>high)
		return -1;

	int mid=(low+high)/2;
	if(array[mid] == data) 
		return mid+1;
	else if(array[mid]>data) {
		high=mid-1;
		return binarySearch(array,low,high,data);
	} else {
		low=mid+1;
		return binarySearch(array,low,high,data);
	}
}

//the programme starts from this point
int main() {
	
	int array[11]={3,5,7,9,10,14,16,35,67,89,90};
	int data,index;
	printf("enter the data to be searched\n");
	scanf("%d",&data);
	index=binarySearch(array,0,10,data);
	if(index!=-1) {

		printf("The data %d is found at position %d in the array \n",data,index);
	} else {
		printf("The data %d is not found in the array \n",data);
	}
	return 0;
}


