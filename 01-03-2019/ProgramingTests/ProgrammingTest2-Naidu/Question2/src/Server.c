//all the headers are included
#include"server.h"

int sockfd,newSockfd,n;
char buf[1024];
char buff[4]={'b','y','e','\n'};

//iot reads the message from client and prints on stdo\ut

void * serverRead(void* arg) {
	while(1){
		memset(buf,0,sizeof(buf));
		n = read(newSockfd,buf,sizeof(buf));
		if(n<0)
			printf("ERROR ON READING");
		if(!(strcmp(buf,buff))) {

			printf("client=%s\n",buf);
			exit(0);
		}
		else 
			printf("client=%s\n",buf);
	}
	return NULL;
}

//it takes input from stdin and writes to client

void * serverWrite(void* arg) {
	while(1)
	{
		memset(buf,0,sizeof(buf));
		n = read(0,buf,1024);
		if(n<0)
			printf("ERROR ON READING");
		n = write(newSockfd,buf,strlen(buf));
		if(n<0)
			printf("ERROR ON WRITING");
		if(!(strcmp(buf,buff)))
			exit(0);
	}
	return NULL;
}

//programe starts here and it takes 2 arguments
int main(int argc,char **argv) {

	pthread_t thread1,thread2;
	struct sockaddr_in serv_addr,cli_addr;
	socklen_t cliLen;
	int r;
	if(argc!=2) {
		printf("enter the arguments in command lind correctly \n");
	}
	sockfd=socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0) {
		printf("error in creating the socket\n");
		exit(0);
	}
	memset((char *)&serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_family=AF_INET;
	serv_addr.sin_addr.s_addr=INADDR_ANY;
	serv_addr.sin_port=htons(atoi(argv[1]));
//it binds the connection
	if(bind(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0) {
		printf("error in binding \n");
		exit(0);
	}
//it listens to no of clients
	listen(sockfd,2);
	cliLen=sizeof(cli_addr);
//connection will hold here until newsockfod is greater than 0
	newSockfd=accept(sockfd,(struct sockaddr *)&cli_addr,&cliLen);
	if(newSockfd<0) {
		printf("error in establishing the connection\n");
		exit(0);
	}
	
//threads to read and write function
	r=pthread_create(&thread1,NULL,serverWrite,NULL);
	if(r<0) {
		printf("error in creating the thread \n");
	}
	r=pthread_create(&thread2,NULL,serverRead,NULL);
	if(r<0) {
		printf("error in creating the thread \n");
	}
//joining of 2 threads
	pthread_join(thread1,NULL);
	pthread_join(thread2,NULL);

//closing sockets
//
	close(newSockfd);
	close(sockfd);
	return 0;
}













