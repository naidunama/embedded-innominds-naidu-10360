//all the headers are included
#include"client.h"

int sockfd,n;
char buf[1024];
char buff[4]={'b','y','e','\n'};

//this functions reads from the socket and displays on STDOUT
void * clientRead(void * arg) {

	while(1) {

		memset(buf,0,sizeof(buf));
		n=read(sockfd,buf,sizeof(buf));
		if(n<0) {
			printf("there is a reading in client side\n");
		}
		if(!(strcmp(buf,buff))) {
			printf("server=%s\n",buf);
			exit(0);
		} else {
			printf("server =%s\n",buf);
		}
	}
	return NULL;
}
//this function takes input from stdin and write to client
void * clientWrite(void *arg) {

	while(1) {


		memset(buf,0,sizeof(buf));
		n=read(0,buf,1024);
		if(n<0) 
			printf("there is rading  problem from input \n");
		
		n=write(sockfd,buf,strlen(buf));
		if(n<0) 
			printf("there is writing problem to client \n");
		if(!(strcmp(buf,buff))) {
			exit(0);
		}
	}
	return NULL;
}

//the progarme starts with main it takes 3 arguments
int main(int argc,char **argv) {

	struct sockaddr_in serv_addr;
	struct hostent *server;
	pthread_t thread1,thread2;
	int r;

	if(argc!=3) {
		printf("enter the inputs correctly in command line \n");
	}
//creates the socket
	sockfd=socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0) {
		printf("error in creating the socket\n");
		exit(0);
	}
//it assignes ip adress
	server=gethostbyname(argv[1]);
	memset((char *)&serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_family=AF_INET;
	strncpy(( char *)server->h_addr,(char *)&serv_addr.sin_addr.s_addr,server->h_length);
	serv_addr.sin_port=htons(atoi(argv[2]));
//it establish the connection between client and server
	if(connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0) {

		printf("error in establishing the connection\n");
		exit(0);
	}
//it creates threads to read and write function
	r=pthread_create(&thread1,NULL,clientWrite,NULL);
	if(r<0) {
		printf("thread is not crearted properly \n");
	}
	r=pthread_create(&thread2,NULL,clientRead,NULL);
	if(r<0) {
		printf("thread is not crearted properly \n");
	}
//threads are joined here untill the complition of 2 threads
	
	pthread_join(thread1,NULL);
	pthread_join(thread2,NULL);

//closing the socket
	close(sockfd);
	return 0;
}


