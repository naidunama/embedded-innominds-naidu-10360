#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct student {

	int roll;
	char name[20];
	struct student *next;
};

void insertAtBeginning(struct student **);
void displayStudentDetails(struct student *);

