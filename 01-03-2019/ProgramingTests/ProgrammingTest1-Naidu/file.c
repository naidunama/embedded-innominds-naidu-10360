#include<stdio.h>
#include<stdlib.h>
int main() {

	int ch;
//creating filepopinter variable
	FILE *fp1,*fp2;
	fp1=fopen("input.txt","r");		
	fp2=fopen("output.txt","w");
//checking file pointer is null or not	
	if(fp1==NULL) {
		printf("cannot oppen the file \n");
		exit(0);
	}else {
		printf("file1 is openend in read mode\n");
	}
	//checking file pointer is null or not
	if(fp2==NULL) {
		printf("cannot oppen the file \n");
		exit(0);
	}else {
		printf("file2 is openend in write mode\n");
	}
//copying the contents of input1 to output file by removing special characters

	while((ch=fgetc(fp1))!=EOF) {
		if(ch==32 || ch==10) {
			fputc('\n',fp2);
			continue;
		}
		if((ch>=65 && ch<=90) || (ch>=97 && ch<=122) ) {
			fputc(ch,fp2);
		}

	}
	fclose(fp1);
	fclose(fp2);
	return 0;
}


