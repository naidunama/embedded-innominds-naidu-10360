#include"header.h"

//it sets the 8th bit in the data
void set8thBit(int data) {

	printf("data before 8th bit is set \n");
	printBinary(data);
	data=data|(1<<8);
	printf("the data after setting the 8th bit is %d\n",data);
	printf("data after the 8th bit is set \n");
	printBinary(data);

}
//it counts the no of set bits in the gievn data
void countSetBits(int data) {
	int c,count=0;
	for(int bit=31;bit>=0;bit--) {
		c=1&(data>>bit);
		if(c==1) {
			count+=1;
		}
	}
	printf("the no of set bits in the given data is %d\n",count);

}
//it calculattes the 2's compiment 
void compliment(int data) {

	int c;
	int arr[8];
	int i=0;

	printf("1's compliment of the giveb number is \n");
	for(int bit=7;bit>=0;bit--) {
		c=1&(~(data>>bit));
		arr[bit]=c;
		printf("%d ",c);
	}
	printf("\n");
	arr[i]+=1;
	while(arr[i]>=2) {
		arr[i]=0;
		i++;
		arr[i]+=1;
	}
	printf(" 2's compliment of given number \n");
	for(int j=7;j>=0;j--) {
		printf("%d ",arr[j]);
	}

	printf("\n");
}

//it prints the given number in the binary format
void printBinary(int data) {
	int c;

	for(int bit=31;bit>=0;bit--) {
		c=1&(data>>bit);
		printf("%d ",c);
	}
	printf("\n");
}

//it displays the operations that are to be performed on data
void menu() {

	printf("------------MENU-------------------\n");
	printf("1.set 8th bit\n2.count no of set bits\n3.compliment\n4.print in binary\n5.exit\n-------------------------\n");
}	


