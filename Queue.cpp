/*----------------------------------------------------------------------------------------------------------------------------------
      
	N.V.V.S NAIDU	10360	naidu.nama@gmail.com		Cell : 7729955884


       PURPOSE
       ---------
     	-The program mainly deals with queue operations which works on principle FIFO where adding at first  position will be deleted first  and displaying the conent in the queue

---------------------------------------------------------------------------------------------------------------------------------
*/
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
using namespace std;

class QueueOperations {

	struct queue {
		int data;		
		struct queue *next;
	}*front,*rear;
	public:

//default constructor
	QueueOperations() {
		front=NULL;
		rear=NULL;
	}
//this methos adds the elements at the end into queue
	void enque(int data) {
		queue *newqueue=new  queue;
		newqueue->data=data;
		if(front==NULL) {
			front=newqueue;
			rear=newqueue;
			newqueue->next=NULL;
			return;
		}
		rear->next=newqueue;
		rear=newqueue;
	}
//this method deletes the first element from the queue
	int deque() {
		int data1;
		if(front==NULL) {
			cout<<"Queue is empty "<<endl;
			return 0;
		}
		queue *temp=front;
		data1=temp->data;
		front=front->next;
		delete temp;
		return data1;
	}
//this method displays the queue contents

	void displayQueue() {
		queue *temp=front;
		cout<<"*****conents in queue:*****"<<endl;
		for(temp=front;temp!=NULL;temp=temp->next) {
			cout<<temp->data<<" ";
		}
		cout<<endl;
	}
	~QueueOperations() {
	
	delete front;
	delete rear;
	}


//this method displays the operations that can be performed on the queue
	void menu() {

		int choice;
		int data;
		while(1) {
			cout<<"**********MENU***********"<<endl;
			cout<<"1.add elements into queue\n2.delete elemet from quueue\n3.display data in queue\n4.quit"<<endl;
			cout<<"enter your choice:"<<endl;
			cin>>choice;
			switch(choice) {
				case 1:
					cout<<"enter the data to be entered into queue:"<<endl;
					cin>>data;
					//adds data into queue
					enque(data);
					break;
				case 2:
					cout<<"deleted  data from the queue is "<<deque()<<endl;

					break;
				case 3:
					//displat conents of queue
					displayQueue();
					break;
				case 4:
					exit(0);
				default:
					cout<<"invalid input"<<endl;
			}
		} 


	}
};

int main() {

	//creating objects for queue
	QueueOperations queue;
	//calling menu method
	queue.menu();
	return 0;
}

/*================================================================================

OUTPUT:

**********MENU***********
1.add elements into queue
2.delete elemet from quueue
3.display data in queue
4.quit
enter your choice:
1
enter the data to be entered into queue:
9
**********MENU***********
1.add elements into queue
2.delete elemet from quueue
3.display data in queue
4.quit
enter your choice:
1
enter the data to be entered into queue:
8
**********MENU***********
1.add elements into queue
2.delete elemet from quueue
3.display data in queue
4.quit
enter your choice:
1
enter the data to be entered into queue:
7
**********MENU***********
1.add elements into queue
2.delete elemet from quueue
3.display data in queue
4.quit
enter your choice:
3
*****conents in queue:*****
9 8 7 
**********MENU***********
1.add elements into queue
2.delete elemet from quueue
3.display data in queue
4.quit
enter your choice:
2
deleted  data from the queue is 9
**********MENU***********
1.add elements into queue
2.delete elemet from quueue
3.display data in queue
4.quit
enter your choice:
3
*****conents in queue:*****
8 7 
**********MENU***********
1.add elements into queue
2.delete elemet from quueue
3.display data in queue
4.quit
enter your choice:
4

*/


