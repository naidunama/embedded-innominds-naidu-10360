/*----------------------------------------------------------------------------------------------------------------------------------

  N.V.V.S NAIDU	10360	naidu.nama@gmail.com		Cell : 7729955884


  PURPOSE
  ---------
  This program is to find the maximun possible sum in the given array
  ----------------------------------------------------------------------------------------------------------------------------------*/

#include<stdio.h>
int main() {
	int array[10]={11,2,-3,4,6,7,-4,3,-2,1};
	int j,a=0,temp=0,currentsum=0,sum=0,b=0,count=0,new=0;;

	//this loop is used when there is only 1 positive elemet or 0 positive eleemnts  in the array
	for(int i=0;i<10;i++) {
		if(array[i]>0) {
			b=array[i];
			break;
		}
	}

	//this loop is used when all the elemts in the array are negative
	if(b==0) {
		b=array[0];
		for(int i=0;i<10;i++) {
			if(b<array[i]) {
				b=array[i];
			}
		}
		printf("the maximum possile sum in an array is %d \n",b);
		return 0;
	}


	for(int i=0;i<10;i++) {
		j=i;
		currentsum=0;
		while(array[j]>=0 && j<10) {
			currentsum=currentsum+array[j];
			j++;
			count++;
		}
		if(count==0) {
			a=b;
		}else {
			a=temp+currentsum;
		}
		if(j<9) {
			temp=currentsum+temp+array[j]+array[j+1];
		}

		if(temp>a) {
			i=j+1;
			new=new+temp;
			if(temp>=sum) {
				sum=temp;
			} else if(new>sum) {
				sum=new;
			}
		} else {
			i=j;
			if(a>=sum) {
				sum=a;
				new=temp-array[j+1];
				temp=0;
			} else if(new>sum) {
				sum=new;
			}
		}
	}
	printf("the maximum possible sum in the given array is %d\n",sum);
	return 0;
}
/*************************output************************


when arr[10]={11,2,-3,4,6,7,-4,3,-2,1} is

  the maximum possible sum in the given array is 27

when arr[10]={111,2,-3,4,6,7,-4,3,-2,46} is

  the maximum possible sum in the given array is 170

when arr[10]={-11,-2,-3,-4,-1,-111,-4,-3,-2,-10} is

  the maximum possible sum in the given array is -1

*/
