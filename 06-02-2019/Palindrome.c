/*----------------------------------------------------------------------------------------------------------------------------------

  N.V.V.S NAIDU	10360	naidu.nama@gmail.com		Cell : 7729955884


  PURPOSE
  ---------
  This program is to find the wheter the number is palindrome or not without using arrays  ----------------------------------------------------------------------------------------------------------------------------------*/

#include<stdio.h>
#include<math.h>

//countDigits method which return the no.of digits present in the given data

int countNoOfDigits(int data) {
	int no_of_digits=0,temp=data;
	while(temp%10) {
		no_of_digits++;
		temp=temp/10;
	}
	return no_of_digits;
}

//isPalindrome method which checks whether the given data is palindrome or not

int isPalindrome(int data) {

	int no_of_digits,temp=data,firstTerm,secondTerm,count=0;

	no_of_digits=countNoOfDigits(data);

	firstTerm=temp%10;
	secondTerm=temp/pow(10,no_of_digits-1);

	while( firstTerm == secondTerm ) {
		count++;
		if(count==no_of_digits/2) {
			return 1;
		}
	firstTerm=temp%10;
	secondTerm=temp/pow(10,no_of_digits-1);

	}
	return 0;
}

int main() {
	
	int data,flag=0;
	
	printf("enter the value to check palindrame or not \n");
	scanf("%d",&data);
	
	//calling the isPalindrome function to check whether it is palindrome or not
	flag=isPalindrome(data);

	if(flag) {
		printf("entered number  %d is palindrome \n",data);
	} else {
		printf("entered num is %d not palindrome\n",data);
	}
	
	return 0;
}
/**********************output*********************
 

enter the value to check palindrame or not
113

entered num is 113 not palindrome

$ ./a.out

enter the value to check palindrame or not
123454321

entered number  123454321 is palindrome
*/

