/*----------------------------------------------------------------------------------------------------------------------------------

  N.V.V.S NAIDU	10360	naidu.nama@gmail.com		Cell : 7729955884


  PURPOSE
  ---------
  This program is to find the number of trailing zeros in the facorial of given number
  ----------------------------------------------------------------------------------------------------------------------------------*/


#include<stdio.h>
#include<math.h>


int main() {
	int n,result=0,temp,p=1,remainder=0;
	printf("enter the number to find trailing zeros of that factorial number  \n");
	scanf("%d",&n);
	temp=n;
	while(temp>=pow(5,p)) {
		remainder=temp/pow(5,p);
		result+=remainder;
		p++;
	}
	printf("the no of trailing zeros in the factorial of  %d is %d \n",n,result);
	return 0;
}
/*************************output*****************
 enter the number to find trailing zeros of that factorial number 

131

the no of trailing zeros in the factorial of  131  is 32 
********************************************************/
