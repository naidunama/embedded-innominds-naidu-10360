/****************************************************************************************************** 
  	
	
  	
	N.V.V.S NAIDU	10360	naidu.nama@gmail.com		Cell : 7729955884

        Purpose:
        --------

        --To Communicate between server and client using sharedmemory and semaphore
        --client uses semaphore  concept

******************************************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <iostream> 
#include <sys/ipc.h> 
#include <sys/shm.h> 
#include <unistd.h>
using namespace std;
// declaaring the global vvariables
static sem_t sem;
static int shmid;
static char *name;

// creating client class
class Client {
	// declaring thread variables
	pthread_t thread1, thread2,thread3;

	public :
	// default clientt constructor
	Client() {
	
		key_t key = ftok("myfile1",85);
		shmid = shmget(key,1024,0666|IPC_CREAT); 


		char  *str = (char*) shmat(shmid,(void*)0,0); 

		cout<<str<<endl;
		shmdt(str);
	}
		
	// method for getting client name 
	static void *clientName(void *arg) {

		sem_wait(&sem);
		
		cout<<"enter client name :"<<endl;
		name=(char*) shmat(shmid,(void*)0,0); 
		cin>>name;
		sleep(10);

		sem_post(&sem);

	}
	// method to create threads and join them
	void clientThread() {

		pthread_create( &thread1, NULL,clientName, NULL);
		pthread_create( &thread2, NULL,clientName, NULL);
		pthread_create( &thread3, NULL,clientName, NULL);
		
	}
	//method for joining the three threads
	void joinThreads() {
		pthread_join( thread1, NULL);
    		pthread_join( thread2, NULL);
		pthread_join( thread3, NULL);

	}

};

int main()
{
	// creating object for client
	Client c;
	// initialising semaphore
	sem_init(&sem, 0, 1);
	// calling client thread
	c.clientThread();
	//calling the joiningclientthread methid
	c.joinThreads();
	// destroying semaphore
	sem_destroy(&sem);
	return 0; 

}
/*================================================================================
// OutPut
//---------------------------

hey whats ur name
enter client name :
arjun
enter client name :
anil
enter client name :
swaroop
*/


