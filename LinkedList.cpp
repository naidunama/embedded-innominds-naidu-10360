/*----------------------------------------------------------------------------------------------------------------------------------
      
	N.V.V.S NAIDU	10360	naidu.nama@gmail.com		Cell : 7729955884


       PURPOSE
       ---------
     	-The program mainly deals with single linked list where adding node at any position and deleting the node at any position and displaying the conent in the list

----------------------------------------------------------------------------------------------------------------------------------*/



#include<iostream>
#include<stdlib.h>
using namespace std;



class SingleLinkedList {


	struct node {
		int data;
		node *next;
	};
	struct node *head;
	int length = 0;

	public:
//default constructor
	SingleLinkedList() {

		head=NULL;

	}
//this method takses the position and data as arugement and adds node into the list

	void addNode(int position,int data) {

		if(position <0 || position > length) {
			cout<<"****************************"<<endl;
			cout<<"cannot add at the position "<<position << " in list which is not there"<<endl;
			cout<<"the length of the list is only "<<length<<endl;
			cout<<"please enter position  between 0 and "<<length<<"only "<<endl;
			cout<<"****************************"<<endl;
		} 
		else if(position==0) {

			node *newnode=new node;

			length++;

			if (head == NULL) {
				head = newnode;
				head->data = data;
				head->next = NULL;
				return;
			}

			newnode->data = data;
			newnode->next = head;
			head = newnode;
		} else if(position==length) {

			node  *temp;
			node *newnode=new node;


			length++;
			temp=head;

			while (temp->next != NULL)
				temp = temp->next;  

			temp->next = newnode;
			newnode->data    = data;
			newnode->next    = NULL;

		}else {

			int index=1;
			node *newnode=new node;
			newnode->data = data;
			node *currentnode=head;
			node *previousnode=head;
			while(index !=position) {
				previousnode=currentnode;
				currentnode=currentnode->next;
				index=index+1;
			}
			previousnode->next=newnode;

			newnode->next=currentnode;


			length++;
		}
	}

//this method takse the position as argument and deletes the node at that position
	void deleteNode(int position) {

		if(position <=0 || position > length) {
			cout<<"cannot delete the position in list which is not there "<<position<<endl;
		} 
		else if(position==1) {
			node *temp=head;
			head=head->next;
			delete temp;
			length=length-1;
		} else {

			int index=1;
			node *currentnode=head;
			node *previousnode=head;
			while(index !=position) {
				previousnode=currentnode;
				currentnode=currentnode->next;
				index=index+1;
			}
			previousnode->next=currentnode->next;
			delete currentnode;
			length--;
		}
	}
//this method updates the data in which is present in specific position in the list
 void updateData(int position) {
                if(position<=0 || position > length) {
                        cout<<"cannot update the data at that position:"<<position<<endl;
                       
                }
                int index;
              	 node *temp=head;
                for(index=1;index<position;index++) {
                        temp=temp->next;
                }
                cout<<"enter the data to be update name:"<<endl;
                cin>>temp->data;
              }

//this method display the contents in the linkedlist
	
	void displayList(void) {

		if(head == NULL) {
			cout<<"the list is empty "<<endl;
		} else {
			node *temp=head;
			while(temp != NULL) {
				cout<<temp->data;
				temp=temp->next;
				cout<<" ";
			}

		}
		cout<<endl;

	}
//destructor 

	~SingleLinkedList() {

		delete head;
	}
//this method shows the operations that can be performed on the linked list

	void  menu() {
		int choice;
		int  data;
		int position;
		while(1) {
			cout<<"1. add an element into linked list."<<endl;
			cout<<"2. Delete the elementfrom the list."<<endl;
			cout<<"3. update the data in the list"<<endl<<"4. display"<<endl<<"5. exit"<<endl;
			cout<<"****************************"<<endl;
			cout<<"enter your choice "<<endl;
			cin>>choice;
			switch(choice) {
				case 1:	cout<<"****************************"<<endl;
					cout<<"enter the position to be added into list "<<endl;
					cin>>position;

					cout<<"Enter value of element"<<endl;
					cin>>data;
					//adds data into list
					addNode(position,data);
					cout<<"****************************"<<endl;
					break;
				case 2:
					cout<<"****************************"<<endl;
					cout<<"enter the position to be deleted "<<endl;
					cin>>position;
					//delete data from the list
					deleteNode(position);
					cout<<"displaying the list elementsafter deletion "<<endl;
					displayList();
					cout<<"****************************"<<endl;
					break;
				case 3:  cout<<"****************************"<<endl;
					cout<<"enter the position to be updated "<<endl;
					cin>>position;
					updateData(position);
					cout<<"displaying the list elements after updation "<<endl;
					displayList();
					cout<<"****************************"<<endl;
					
				case 4:
					cout<<"****************************"<<endl;
					cout<<"displaying the list elements"<<endl;
					//display contents of list
					displayList();
					cout<<"****************************"<<endl;
					break;
			
				case 5:exit(0);
				default :
				       cout<<"Please enter valid input"<<endl;      
			}

		}
	}
};


int main() {
	

//object is created for singlelinkedlist class
	SingleLinkedList list;
// calls the main method
	list.menu();
	return 0;
}

/*================================================================================

OUTPUT:
1. add an element into linked list.
2. Delete the elementfrom the list.
3. display
4.exit
****************************
enter your choice 
1
****************************
enter the position to be added into list 
0
Enter value of element
9
****************************
1. add an element into linked list.
2. Delete the elementfrom the list.
3. display
4.exit
****************************
enter your choice 
1
****************************
enter the position to be added into list 
1
Enter value of element
8
****************************
1. add an element into linked list.
2. Delete the elementfrom the list.
3. display
4.exit
****************************
enter your choice 
1
****************************
enter the position to be added into list 
0
Enter value of element
7
****************************
1. add an element into linked list.
2. Delete the elementfrom the list.
3. display
4.exit
****************************
enter your choice 
3
****************************
displaying the list elements
7 9 8 
****************************
1. add an element into linked list.
2. Delete the elementfrom the list.
3. display
4.exit
****************************
enter your choice 
1
****************************
enter the position to be added into list 
2
Enter value of element
6
****************************
1. add an element into linked list.
2. Delete the elementfrom the list.
3. display
4.exit
****************************
enter your choice 
3
****************************
displaying the list elements
7 6 9 8 
****************************
1. add an element into linked list.
2. Delete the elementfrom the list.
3. display
4.exit
****************************
enter your choice 
2
****************************
enter the position to be deleted 
3
displaying the list elementsafter deletion 
7 6 8 
****************************
1. add an element into linked list.
2. Delete the elementfrom the list.
3. display
4.exit
****************************
enter your choice 
4


*/







