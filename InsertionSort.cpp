#include<iostream>
#include<stdlib.h>
using namespace std;

class InsertionSort {

	public :

		void inputArray(int *array,int size) {
			cout<<"enter the elements into array"<<endl;
			for(int i=0;i<size;i++) {
				cin>>array[i];
			}

		}




		void insertionSort(int *array,int size) {

			int temp,j;
			for(int i=1;i<size;++i)
			{
				j=i-1;
				temp=array[i];
				while( j>=0 && array[j]>temp)
					
					{
						array[j+1]=array[j];
						--j;
					}
					array[j+1]=temp;
			}

		}
		void displayArrayElements(int *array,int size) {
			for(int i=0;i<size;i++) {
				cout<<array[i]<<" ";
			}
			cout<<endl;
		}

};

int main() {

	int *array=NULL;

	int size;
	cout<<"enter the size of array "<<endl;
	cin>>size;
	while(size<=0) {
	cout<<"array size cannot be negative and 0......please renter "<<endl;
	cin>>size;
	}
	array=new int[size];
	InsertionSort i;
	i.inputArray(array,size);
	cout<<"array elements before sorting "<<endl;
	i.displayArrayElements(array,size);
	i.insertionSort(array,size);
	cout<<"array elements after using insertion sorting "<<endl;
	i.displayArrayElements(array,size);
}
/*********************************************************************************************************
OUTPUT:
-------

enter the size of array 
-3
array size cannot be negative and 0......please renter 
6
enter the elements into array
9
6
1
4
3
2
array elements before sorting 
9 6 1 4 3 2 
array elements after using insertion sorting 
1 2 3 4 6 9 
*/
