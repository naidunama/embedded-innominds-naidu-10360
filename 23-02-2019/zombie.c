/*****************************************************************************************

N.V.V.S NAIDU       10360        naidu.nama@gmail.com          cell:7729955884

PURPOSE:

	~this programe is implementation of zombie process(parent doesn't collect the status of child..then the child is known as zombie)

*****************************************************************************************/
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>

int main()
{
	int ret = -1, pid;
	printf("Hello\n");
	ret = fork();
	if(ret == 0)
	{
		printf("inside child process\n");
		pid = getpid();
		printf("(child)child process id: %d\n", pid);
		printf("child going to sleep\n");
		sleep(2);
		printf("child woke up\n");
		printf("child terminated\n");
	}
	else
	{
		
		printf("parent\n");
		printf("inside parent process\n");
		printf("(parent)child process id: %d\n", ret);
		printf("parent process id: %d\n", getpid());
		printf("parent going to sleep\n");
		sleep(5);
		printf("parent woke up\n");
		printf("parent terminated\n");
	}
	

	return 0;
}

/*************************OUTPUT******************************************
Hello
parent
inside parent process
(parent)child process id: 8122
parent process id: 8121
parent going to sleep
inside child process
(child)child process id: 8122
child going to sleep
child woke up
child terminated
parent woke up
parent terminated
*************************************************************************/
