/*****************************************************************************************

N.V.V.S NAIDU       10360        naidu.nama@gmail.com          cell:7729955884

PURPOSE:

	~this programe is implementation of orphan process(parent dies but still child process executes..then the child is known as zombie)

*****************************************************************************************/
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>

int main()
{
	int ret = -1, pid;
	printf("Hello\n");
	//2 process are created
	ret = fork();
	if(ret == 0)
	{
		printf("inside child process\n");
		pid = getpid();
		printf("(child)child process id: %d\n", pid);
		printf("child going to sleep\n");
		sleep(5);
		printf("child woke up\n");
		printf("(child)child parent process id: %d\n", getppid());
		printf("child terminated\n");
	}
	else
	{
		
		printf("parent\n");
		printf("inside parent process\n");
		printf("(parent)child process id: %d\n", ret);
		printf("parent process id: %d\n", getppid());
		printf("parent terminated\n");
	}
	

	return 0;
}

/**********************OUTPUT******************************************
Hello
parent
inside parent process
(parent)child process id: 8172
parent process id: 1903
parent terminated
inside child process
(child)child process id: 8172
child going to sleep
vidya@vidya-Inspiron-5558:~/ctraining$ child woke up
(child)child parent process id: 1356
child terminated
******************************************************************/
