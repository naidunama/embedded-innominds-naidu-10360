/*****************************************************************************************

  N.V.V.S NAIDU       10360        naidu.nama@gmail.com          cell:7729955884

PURPOSE:

~this programe is implementation of forking techinque

 *****************************************************************************************/
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>

int main()
{
	int ret = -1, pid,child_exit_status;
	printf("Hello\n");
	//2 process are created
	ret = fork();
	if(ret == 0)
	{
		printf("inside child process\n");
		pid = getpid();
		printf("(child)child process id: %d\n", pid);
		printf("child going to sleep\n");
		sleep(5);
		printf("child woke up\n");
		printf("(child)child parent process id: %d\n", getppid());
		printf("child terminated\n");
		exit(3);
	}
	//parent process waiting untill the child completes
	wait(&child_exit_status);
	printf("parent\n");
	printf("inside parent process\n");
	printf("(parent)child process id: %d\n", ret);
	printf("parent process id: %d\n", getppid());
	printf("parent terminated\n");


	return 0;
}

/**********************OUTPUT******************************************
 
Hello
inside child process
(child)child process id: 8244
child going to sleep
child woke up
(child)child parent process id: 8243
child terminated
parent
inside parent process
(parent)child process id: 8244
parent process id: 1903
parent terminated

**********************************************************************/
